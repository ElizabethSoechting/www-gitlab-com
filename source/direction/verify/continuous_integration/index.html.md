---
layout: markdown_page
title: "Category Direction - Continuous Integration"
---

- TOC
{:toc}

## Continuous Integration

Continuous Integration is an important part of any software development pipeline. It must be easy to use, reliable, and accurate in terms of results, so that's the core of where we focus for CI. While we are very proud that GitLab CI/CD is recognized as [the leading CI/CD tool on the market](/blog/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), as well as a leader in the 2019 Q3 [Cloud Native CI Wave](/resources/forrester-wave-cloudnative-ci/), it's important for us that we continue to innovate in this area and provide not just a "good enough" solution, but a great one.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration)
- [Overall Vision](/direction/cicd#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

There are a couple related areas you may be looking for:

- Because CI is such a broad category, a lot of the [CI/CD vision and themes](https://about.gitlab.com/direction/cicd/) are directly applicable here. Since this page is a subset of that one we don't duplicate the content here, but it's also worth reading if you're interested in how we're thinking about CI/CD at GitLab especially over the medium to long term.
- The GitLab Runner is managed by the Verify team also, and is a key component of the CI solution. It has its own strategy that can be found on its [direction page](/direction/verify/runner/)
- Continuous Delivery (and Progressive Delivery) is an important topic that is intimately bound to CI. You can read more about our strategy for CD at our [direction page](/direction/release/continuous_delivery/), and more broadly about our strategy for releasing software at our [Release stage direction page](/direction/release/).

## What's Next & Why

We recently introduced the [directed acylic graph](https://docs.gitlab.com/ee/ci/directed_acyclic_graph) and [child/parent pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html), which provide a ton of flexibility around how jobs and stages relate to each other and are super valuable for monorepo type use cases. We want to expand on this next by introducing the ability to generate a new `.gitlab-ci.yml` on the fly and then allow you to [trigger it dynamically](https://gitlab.com/gitlab-org/gitlab/issues/35632). This can be used for a lot of different use cases where you need to control behavior at runtime, including things like matrix builds, additional monorepo behaviors, and so on.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our [definitions of maturity levels](/direction/maturity/)), it is important to us that we defend that position in the market. As such, we are balancing prioritization of [important P2 issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=P2) and [items from our backlog of popular smaller feature requests](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Acontinuous+integration&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93) in addition to delivering new features that move the vision forward. If you feel there are any gaps or items that would risk making GitLab no longer lovable for you, please let us know by contacting [Jason Yavorska](https://gitlab.com/jyavorska) (PM for this area) with your ideas.

## Competitive Landscape

To be more competitive we are considering [gitlab#10107](https://gitlab.com/gitlab-org/gitlab/issues/10107) to move GitLab CI/CD for External Repos (GitHub + BitBucket) from Premium to Starter. This will help more teams have access to GitLab CI/CD, and is potentially a great way to introduce more people to GitLab in general, but also comes with costs of running the CI/CD so the decision needs to be weighed closely against the opportunity. Note that in the meantime, on gitlab.com, this feature [remains in core](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/) and is free for you to use.

### Microsoft

The most urgent gap that we want to address in competing with Microsoft is the capability to have Windows Runners as part of our shared fleet. This feature is now available in beta, and we are working towards our general launch. Additional details can be found on our [runner category vision](/direction/verify/runner/).

#### GitHub CI/CD (Actions)

GitHub has evolved Actions into more and more of a standalone CI/CD solution. GitLab remains far ahead in a lot of areas of CI/CD that they are going to have to catch up on, but Microsoft and GitHub have a lot of resources and have a big user base ready and excited to use their free product. Making an enterprise-ready solution will take some time, but they are no doubt actively working on closing these gaps.

There are two main elements to Actions that are comparable to GitLab. The first is the event-based nature which allows for flexible wiring of components together, even across systems. In some ways this is powerful and allows you to quickly set up different integrated workflows that can trigger based on different things happening in the system. We do provide some of the same events, but not complete flexibility. It remains to be seen if this ends up being a stable long term way to manage a CI system, however, since stability, auditing, and so on are so important, for now we are monitoring.

The other element is the suite of open source individual Actions that perform single tasks, and are embeddable within pipelines. This comes with some of the downside of the Jenkins plugin ecosystem where you are depending on maintainers and others out of your purview, but because Actions is new it has for the moment a ton of engaged people actively producing and maintaining them. CircleCI offers something very similar with Orbs. At the moment we are investigating options here, including [making it easy for Actions or Orbs to run on GitLab](https://gitlab.com/gitlab-org/gitlab/issues/195173#note_271774504) and/or working on an open standard.

#### Azure DevOps

Microsoft has provided a [feature comparison between Azure DevOps (though not GitHub CI/CD) and GitLab](https://docs.microsoft.com/en-us/azure/devops/learn/compare/azure-devops-vs-gitlab) which, although it is not 100% accurate, highlights the areas where they see an advantage over us. Our responses overall as well as to the inaccuracies can be found in [our comparison page](azure_devops_detail.html).

### CloudBees Jenkins/CodeShip

Jenkins has gained a reputation for being original innovators, but now dated and bound to legacy concepts and code. They have recognized this and are trying to address the issue by [splitting](https://jenkins.io/blog/2018/08/31/shifting-gears/) their product into multiple offerings, giving them the freedom to shed some technical and product debt and try to innovate again. They also acquired CodeShip, a SaaS solution for CI/CD so that they can play in the SaaS space. All of this so far creates a complicated message that doesn't seem to be resonating with analysts or customers yet.

### Bitbucket Pipelines and Pipes

BitBucket pipelines has been Atlassian's answer to a more integrated CI/CD approach than Atlassian Bamboo, is UI driven, and enables users to build and deploy code by creating YAML based configuration files. On February 28, 2019, Atlassian announced [Bitbucket Pipes](https://bitbucket.org/blog/meet-bitbucket-pipes-30-ways-to-automate-your-ci-cd-pipeline) as an evolution of Bitbucket pipelines and gained a lot of press around [Atlassian taking on GitHub/Microsoft](https://www.businessinsider.com/atlassian-bitbucket-pipes-ci-cd-gitlab-github-2019-2). While it is early in the evolution of this as a product in the enterprise market, there are a number of interesting patterns in the announcement that seem to favor Convention over Configuration.

### CodeFresh

CodeFresh follows a similar [container-based plugin model](https://steps.codefresh.io/) as GitHub Actions, so our approach is similar to the one written above in response to that solution.

### CircleCI

CircleCI Orbs are also essentially the same as GitHub Actions, so for our response thre please see above. The other thing that CircleCI is great at is fast availability of Runners, as well as startup time. How we are working to improve this can be found on our [runner category vision](/direction/verify/runner/).

## Analyst Landscape

There are a few key findings from the Forrester Research analysts on our CI solution:

- In terms of Cloud Native CI, GitLab's solution is seen as capable as the solutions provided by the hyperclouds themselves, and well ahead of other neutral solutions. This can give our users flexibility when it comes to which cloud provider(s) they want to use.
- GitLab is seen as the best end to end leader where other products are not keeping up and not providing a comprehensive solution. We should continue to build a deep solution here in order to stay ahead of competitor's solutions.
- Competitors claim that GitLab can go down and doesn't scale, which is perhaps their best argument if their products do not provide as comprehensive solutions. This isn't really true, so we need to have messaging and product features that make this clear.

To continue to drive innovation in this area, we are considering [gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306) next to add Vault integration throughout the CI pipeline. This builds off of work happening on the Configure team and will allow for a more mature delivery approach that takes advantage of ephemeral credentials to ensure a rock solid secure pipeline.

## Top Customer Success/Sales Issue(s)

The most popular Customer Success issues as determined in FQ1-20 survey of the Technical Account Managers was [filtering pipelines by status or branch](https://gitlab.com/gitlab-org/gitlab/issues/15268).

## Top Customer Issue(s)

Our most popular customer issue is to add an option to keep only the latest artifact in each branch ([gitlab#16267](https://gitlab.com/gitlab-org/gitlab/issues/16267)). Another item with a lot of intention include normalizing job tokens in a more flexible way, so that they can have powerful abilities when needed and still not introduce security risks ([gitlab#3567](https://gitlab.com/gitlab-org/gitlab/issues/35067)), 

We also have a few issues about making variables avaiable before includes are processed, however there is a "chicken and egg" problem here that has been difficult to solve. Child/parent pipelines solves some use cases, but not all, and in the meantime we are continuing the discussion in the issue [gitlab#1809](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809). If you're interested in technical discussion around the challenges and want to participate in solving them, please see the conversation [here](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809#note_225636231).

## Top Internal Customer Issue(s)

Our top internal customer issues include the following:

* Showing a high visiblity alert if master is red ([gitlab#10216](https://gitlab.com/gitlab-org/gitlab/issues/10216))
* Allow `needs` to specify a job that can be started immediately ([gitlab#30631](https://gitlab.com/gitlab-org/gitlab/issues/30631))
* Filters for pipelines ([gitlab#15268](https://gitlab.com/gitlab-org/gitlab/issues/15268))
* Recognizing links in job logs ([gitlab#18324](https://gitlab.com/gitlab-org/gitlab/issues/18324))
* Visualizing job duration ([gitlab#2666](https://gitlab.com/gitlab-org/gitlab/issues/2666))

## Top Vision Item(s)

Our most important vision items are our follow ups for the Directed acyclic graphs (DAG) for pipelines MVC [gitlab-org#1716](https://gitlab.com/groups/gitlab-org/-/epics/1716), which will allow for out of order execution and open a world of new possibilities around how pipelines are constructed.

[Cross-project triggered-by registry image change](https://gitlab.com/gitlab-org/gitlab/issues/10074) is also an interesting one for our vision, which would add the ability to depend on container image changes in another project instead of pipelines. This could be a nicer, container-first way of working.
