---
layout: markdown_page
title: "Category Direction - Dependency Scanning"
---

- TOC
{:toc}

## Description

### Overview

Dependency Scanning is a technique that identifies project dependencies and checks if there are any known, publicly disclosed, vulnerabilities that may affect the main application.

Applications define which package they require, and which is the version that is used. Dependency Scanning leverage a database of known vulnerabilities to check if any of these dependencies are not secure, and it notifies that a package upgrade is needed.

Dependency Scanning is very dependent not only on the programming languages, but also on the package manager. Different package managers have different repositories and ways to keep track of versions.

### Goal

Our goal is to provide Dependency Scanning as part of the standard development process. This means that Dependency Scanning is executed every time a new commit is pushed to a branch. We also include Dependency Scanning as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

We also want to make Dependency Scanning complexity totally transparent to users. GitLab is able to automatically detect the package manager and to extract the information. We want to increase language coverage by including support for the most common languages.

GitLab should also be able to check if a vulnerable function is really used by the application, and provide such information to prioritize better.

Dependency Scanning results can be consumed in the merge request, where only new vulnerabilities, introduced by the new code, are shown. A full report is available in the pipeline details page.

Dependency Scanning results are also part of the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can check the security status.

[Auto Remediate](https://docs.gitlab.com/ee/user/project/merge_requests/#solutions-for-dependency-scanning-ultimate) leverages Dependency Scanning to provide a solution for vulnerabilities that can be applied to fix the codebase. It will be automatically done by GitLab in the future.

Dependency Scanning can also be included in a bill of materials (BOM), where all the components are listed with their security status. See https://gitlab.com/gitlab-org/gitlab-ee/issues/7476 for additional details.

### Roadmap

- [First MVC (already shipped)](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [Dependency Scanning improvements](https://gitlab.com/groups/gitlab-org/-/epics/298)

## What's Next & Why

We want people to always have the most recent job definition for Dependency Scanning. This is very important to get benefits from all the new updates we are shipping. Auto DevOps is a solution for that, but users may need to have explicit jobs for their projects. That's why it is important to provide a simple way to keep definitions up to date.

The next MVC is to support [multi-module Maven projects out of the box](https://gitlab.com/gitlab-org/gitlab-ee/issues/6425)

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/529)

## Competitive Landscape

- [Black Duck](https://www.blackducksoftware.com/solutions/application-security)
- [CA Veracode](https://www.veracode.com/security/)
- [Sonatype Nexus](https://www.sonatype.com/nexus-auditor)
- [Whitesource](https://www.whitesourcesoftware.com/open-source-security/)

## Analyst Landscape

The Dependency Scanning topic is often coupled with License Compliance in Software Composition Analysis (SCA). This is what analysts evaluate, and how it is bundled in other products. As defined in our [Solutions](https://about.gitlab.com/handbook/product/categories/index.html#solutions), GitLab includes Container Scanning as part of Software Composition Analysis.

We should make sure that we can address the entire solution even if we consider these features as independent, and to leverage the single application nature of GitLab to provide a consistent experience in all of them.

- [Forrester](https://www.forrester.com/report/The+Forrester+Wave+Software+Composition+Analysis+Q1+2017/-/E-RES136463)

Analysts are showing interest for Auto Remediation as the key feature to make dependency scanning really actionable for users. We can invest to [increase our coverage](https://gitlab.com/gitlab-org/gitlab-ee/issues/9385).

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=analysts&label_name[]=dependency%20scanning)

## Top Customer Success/Sales Issue(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/6772)
- [Issue 2](https://gitlab.com/gitlab-org/gitlab-ee/issues/7115)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name%5B%5D=customer&label_name%5B%5D=dependency%20scanning)

## Top user issue(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/6772)
- [Issue 2](https://gitlab.com/gitlab-org/gitlab-ee/issues/7115)
- [Issue 3](https://gitlab.com/gitlab-org/gitlab-ee/issues/6425)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=popularity&label_name%5B%5D=dependency%20scanning)

## Top internal customer issue(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/9197)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=internal%20customer&label_name[]=dependency%20scanning)

## Top Vision Item(s)

- [Issue 1](https://gitlab.com/groups/gitlab-org/-/epics/133)
- [Issue 2](https://gitlab.com/gitlab-org/gitlab-ee/issues/8213)
